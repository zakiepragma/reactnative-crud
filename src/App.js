import {ScrollView, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import LocalAPI from './pages/LocalAPI';

const App = () => {
  return (
    <View>
      <ScrollView>
        <LocalAPI />
      </ScrollView>
    </View>
  );
};

export default App;

const styles = StyleSheet.create({});
