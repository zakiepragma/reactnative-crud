import {
  Alert,
  Button,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Axios from 'axios';

const LocalAPI = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [bidang, setBidang] = useState('');
  const [users, setUsers] = useState([]);
  const [button, setButton] = useState('Simpan');
  const [selectedUser, setSelectedUser] = useState({});
  const [ubahWarnaButton, setUbahWarnaButton] = useState('');

  useEffect(() => {
    getData();
  }, []);

  const submit = () => {
    const data = {name, email, bidang};
    if (button === 'Simpan') {
      Axios.post('http://10.0.2.2:3004/users', data).then(res => {
        setName('');
        setBidang('');
        setEmail('');
        getData();
      });
    } else if (button === 'Update') {
      Axios.put(`http://10.0.2.2:3004/users/${selectedUser.id}`, data).then(
        res => {
          setName('');
          setBidang('');
          setEmail('');
          getData();
          setButton('Simpan');
          setUbahWarnaButton('');
        },
      );
    }
  };

  const getData = () => {
    Axios.get('http://10.0.2.2:3004/users').then(res => {
      console.log('res: ', res);
      setUsers(res.data);
    });
  };

  const selectItem = item => {
    console.log('select ', item);
    setSelectedUser(item);
    setName(item.name);
    setEmail(item.email);
    setBidang(item.bidang);
    setButton('Update');
    setUbahWarnaButton('green');
  };

  const deleteItem = item => {
    Axios.delete(`http://10.0.2.2:3004/users/${item.id}`).then(res => {
      getData();
    });
  };

  const clear = () => {
    setName('');
    setBidang('');
    setEmail('');
    setButton('Simpan');
    setUbahWarnaButton('');
  };

  return (
    <View style={styles.container}>
      <Text style={styles.textTitle}>Pragma Developer</Text>
      <Text style={styles.textPragma}>Masukkan Anggota Pragma Informatika</Text>
      <TextInput
        style={styles.input}
        placeholder="Nama Lengkap"
        value={name}
        onChangeText={value => setName(value)}
      />
      <TextInput
        style={styles.input}
        placeholder="Email"
        value={email}
        onChangeText={value => setEmail(value)}
      />
      <TextInput
        style={styles.input}
        placeholder="Bidang"
        value={bidang}
        onChangeText={value => setBidang(value)}
      />
      <View style={styles.action}>
        <View style={styles.simpan}>
          <Button title={button} onPress={submit} color={ubahWarnaButton} />
        </View>
        <View style={styles.clear}>
          <Button title="Clear" onPress={clear} color="red" />
        </View>
      </View>
      <View style={styles.line} />
      {users.map(user => {
        return (
          <Item
            key={user.id}
            name={user.name}
            email={user.email}
            bidang={user.bidang}
            onPress={() => selectItem(user)}
            onDelete={() =>
              Alert.alert('Peringatan', 'Anda yakin akan menghapus data ini?', [
                {text: 'Tidak', onPress: () => console.log('button tidak')},
                {text: 'Ya', onPress: () => deleteItem(user)},
              ])
            }
          />
        );
      })}
    </View>
  );
};

export default LocalAPI;

const Item = ({name, email, bidang, onPress, onDelete}) => {
  return (
    <View style={styles.itemContainer}>
      <TouchableOpacity onPress={onPress}>
        <Image
          source={{
            uri: `https://ui-avatars.com/api/?name=${name}?background=random`,
          }}
          style={styles.avatar}
        />
      </TouchableOpacity>
      <View style={styles.desc}>
        <Text style={styles.descName}>{name}</Text>
        <Text style={styles.descEmail}>{email}</Text>
        <Text style={styles.descBidang}>{bidang}</Text>
      </View>
      <TouchableOpacity onPress={onDelete}>
        <Text style={styles.delete}>X</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {padding: 20},
  textTitle: {textAlign: 'center', marginBottom: 20},
  textPragma: {marginBottom: 10},
  line: {height: 2, backgroundColor: 'black', marginVertical: 20},
  input: {
    borderWidth: 1,
    marginBottom: 12,
    borderRadius: 25,
    paddingHorizontal: 18,
  },
  avatar: {width: 80, height: 80, borderRadius: 80},
  desc: {marginLeft: 18, flex: 1},
  descEmail: {fontSize: 16},
  descName: {fontSize: 20, fontWeight: 'bold'},
  descBidang: {fontSize: 12, marginTop: 8},
  itemContainer: {flexDirection: 'row', marginBottom: 20},
  delete: {fontSize: 20, fontWeight: 'bold', color: 'red'},
  action: {flexDirection: 'row', justifyContent: 'space-around'},
  simpan: {
    borderRadius: 20,
    overflow: 'hidden',
    width: 250,
  },
  clear: {
    borderRadius: 20,
    overflow: 'hidden',
    width: 100,
  },
});
